<?php
namespace System\Tools;

use DateTime;

class Time{
    private static \DateTime $dateTime;

    static function updateDateTime(){
        self::$dateTime = new DateTime(date("Y-m-d H:i:s"));
    }
    public static function getDateTime():\DateTime{
        return self::$dateTime;
    }
}