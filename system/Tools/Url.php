<?php

namespace System\Tools;

class Url{
    private string $baseUrl = '';
    private string $appDir = '';

    public function __construct(string $url, string $appDir)
    {
        $this->setBaseUrl($url);
        $this->appDir = $appDir;
    }
    public function setBaseUrl(string $url){
        $this->baseUrl = $url;
    }

    public function toRoute(string $route):string{
        return trim($this->baseUrl,'/').'/'.trim($route,'/');
    }

    public function toPath(string $path):string{
        return '/'.trim($this->appDir,'/').'/'.trim($path,'/');
    }
    
}