<?php

use System\Tools\Session;

function view(string $path, array $paramns = []):string{
    return System\Tools\Utils::view($path,$paramns);
}
function url():System\Tools\Url{
    return new System\Tools\Url(APP_URL,APP_DIR);
}

function security():System\Tools\Security{
    return new System\Tools\Security(APP_SALT);
}

function auth():System\Server\Authentication{
    return new System\Server\Authentication(App\Users\Rules\Authentication\Authentication::class);
}

function session():System\Tools\Session{
    return System\Tools\Session::instance();
}

function response():System\Server\Response{
    return new System\Server\Response();
}

function obfuscateJs(string $js):string{
    return (new \Tholu\Packer\Packer($js, 'Normal', true, false, true))->pack();
}