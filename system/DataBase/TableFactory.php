<?php
namespace System\DataBase;

use System\Configs\DataBase;
use System\DataBase\Builders\Table\ABuilder;
use System\DataBase\Builders\Table\Mysql;
use System\DataBase\Drivers\ADriver;
use System\DataBase\Drivers\Mysqli;
use System\DataBase\Entities\Table;

class TableFactory extends Table{

    private ADriver $driver;
    private ABuilder $builder;
    public function __construct(ADriver $driver,ABuilder $builder, string $name)
    {
        parent::__construct($name);
        $this->driver = $driver;
        $this->builder = $builder;
    }

    public function create():void{
        $this->driver->query($this->builder->create($this))->execute();
    }
    
    public static function mysqli(string $name,DataBase $config = null):TableFactory{
        if($config === null){
            $config = DataBase::default();
        }
        $driver = new Mysqli($config);
        $builder = new Mysql();
        return new TableFactory($driver,$builder,$name);
    }
}