<?php
namespace System\Models;

use System\Exceptions\ValidationException;

class Validation{


    private array $validations = [];

    public function addValidation(string $name, string $validation){
        $this->validations[$name] = $validation;
    }

    public function validate(AModel $model){
        $errors = [];
        foreach($model->getFields() as $field){
            
            if(!isset($this->validations[$field])){
                continue;
            }
            $namespace = $this->validations[$field];
            $instance = new $namespace((string)($model->{$field}));
            if(!$instance->validate()){
                $errors[$field] = $instance->getMessage();
            }

        }
        if($errors){
            throw new ValidationException('Validation error',$errors);
        }
    }
    
}