<?php
namespace System\Models\Validations;

class Required extends AValidation{

    function validate():bool{
        return (bool)$this->data;
    }
}