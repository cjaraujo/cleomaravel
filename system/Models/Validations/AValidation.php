<?php
namespace System\Models\Validations;

abstract class AValidation{
    protected string $data;

    protected string $message = '';

    public function __construct(string $data)
    {
        $this->data = $data;
    }

    abstract function validate():bool;

    public function getMessage():string{
        return $this->message;
    }
}