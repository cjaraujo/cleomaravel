<?php
namespace System\Models;

abstract class AModel{
    protected array $fields = [];
    protected array $values = [];

    public function setValue(string $field, string $value = null):self{
        if(!in_array($field,$this->fields)){

        }
        $this->values[$field] = $value;
        return $this;
    }

    public function getValue(string $field){
        if(!in_array($field,$this->fields)){

        }
        if(!isset($this->values[$field])){
            return null;
        }
        return $this->values[$field];
    }
    public function __get($field)
    {
        return $this->getValue($field);
    }
    public function __set(string $field, string $value = null){
        $this->setValue($field,$value);
    }

    public function setValues(array $values){
        foreach($values as $field=>$value){
            $this->setValue($field,$value);
        }
    }

    public function getFields():array{
        return $this->fields;
    }

    public function toArray():array{
        return $this->values;
    }
}