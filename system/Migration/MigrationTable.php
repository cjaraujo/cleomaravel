<?php
namespace System\Migration;

use System\DataBase\TableFactory;

class MigrationTable{
    protected string $table = 'migration';
    public function up(){
        $table = TableFactory::mysqli($this->table);
        $table->addInt('id')->size('11')->autoincrement();
        $table->addVarchar('file')->size('128');
        $table->addDatetime('insert_at');
        $table->primaryKey('id');
        $table->create();
    }
}