<?php
namespace System\Migration;
use System\Migration\Models\MigrationModel;
use System\Tools\Time;

class Migration{

    private MigrationModel $model;

    public function __construct()
    {
        $this->model = MigrationModel::instance();
        (new MigrationTable)->up();
    }
    public function update(){
        $migrations = $this->model->result();
        $classes = $this->getMigrationClasses(array_map(function($data){return $data->file;},$migrations));
        ksort($classes);
        foreach($classes as $file=>$class){
            try{
                echo $class->up(); 
                echo PHP_EOL.'------------------------------------------------'.PHP_EOL;
                $this->model->id = null;
                $this->model->file = addslashes($file);
                $this->model->insert_at = Time::getDateTime()->format('Y-m-d H:i:s');
                $this->model->insert();
            }catch(\Exception $e){
                echo $e->getMessage();
                break;
            }

        }
    }

    private function getMigrationClasses(array $migrations):array{
        $files = glob(APP_DIR."/app/*/Migration/*.php");
        $classes = [];
        
        foreach($files as $file){
            $file = str_replace(APP_DIR."/app/",'',$file);
            $path = explode('/',$file);
            $className = "App\\{$path[0]}\Migration\\".basename($file,'.php');
            if(in_array($className,$migrations)){
                continue;
            }
            $classes[$className] = new $className;
        }
        return $classes;
    }
}