<?php
namespace System\Migration\Models;

use System\Models\TableModel;

class MigrationModel extends TableModel{
    protected string $table = 'migration';
    protected array $fields = ['id','file','insert_at'];
    protected array $primaryKeys = ['id'];

}