<?php
namespace System\Server;
class Response{

    const HTTP_STATUS = ['422'=>'422 Unprocessable Entity'];

    public function html($contents, $httpCode = 200){
        exit($contents);
    }

    public function json($contents, $httpCode = 200){
        header($_SERVER['SERVER_PROTOCOL'] . ' '.self::HTTP_STATUS[$httpCode], true, $httpCode);
        header('Content-Type: application/json');
        exit(json_encode($contents));
    }

    public function redirect($url){
        header("Location: $url");
        exit;
    }
    
}