<?php
namespace System\Server;

use System\Server\Entities\Route;
use System\Server\Entities\Request;

class Routes{
    private array $loadedRoutes = [];

    public function get(string $route, string $controller, string $method, string $scope = ""):self{
        $this->loadedRoutes['GET'][] = ['route'=>trim($route,'/'),'controller'=>$controller,'method'=>$method,'scope'=>$scope];
        return $this;
    }
    public function post(string $route, string $controller, string $method, string $scope = ""):self{
        $this->loadedRoutes['POST'][] = ['route'=>trim($route,'/'),'controller'=>$controller,'method'=>$method,'scope'=>$scope];
        return $this;
    }
    public function put(string $route, string $controller, string $method, string $scope = ""):self{
        $this->loadedRoutes['PUT'][] = ['route'=>trim($route,'/'),'controller'=>$controller,'method'=>$method,'scope'=>$scope];
        return $this;
    }

    public function load(Request $request){
        $this->loadedRoutes = [];
        $this->loadModule($request->getCalledModule());
        return $this->loadRoute($request);
    } 
    private function loadModule($module){
        $namespace = "App\\{$module}\Routes\Web";
        if(!class_exists($namespace)){
            //erro            
        }
        $instance = new $namespace;
        $instance->load($this);
    }
    private function loadRoute(Request $request){
        $segments = explode("/", $request->getEndpoint());
        foreach ((array)$this->loadedRoutes[$request->getVerb()] as $route) {
            $routeFound = true;
            $uriVariables= [];
            $path = explode("/", $route['route']);
            foreach ($path as $index => $valor) {
                if (!isset($segments[$index])) {
                    $routeFound = false;
                    break;
                }
                if (strpos($valor, "{") !== false && strpos($valor, "}") !== false) {
                    $variableKey = str_replace(["{", "}"], "", $valor);
                    $uriVariables[$variableKey] = $segments[$index];
                    continue;
                }
                if ($valor != $segments[$index]) {
                    $routeFound = false;
                    break;
                }
            }
            if ($routeFound) {
                
                $routeObject =  new Route();
                $routeObject->setArgs($uriVariables);
                $routeObject->setController($route['controller']);
                $routeObject->setMethod($route['method']);
                $routeObject->setScope($route['scope']);
                return $routeObject;
            }
        }
        //erro
    }

}
