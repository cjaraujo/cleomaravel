<?php
namespace System\Server;

use System\Server\Entities\Auth;
use System\Server\Entities\Route;
use System\Server\Interfaces\IAuthentication;

class Authentication{
    

    private string $authenticationNameSpace = "";

    private Auth $auth;

    public function __construct(string $authenticationNameSpace)
    {
        if(!class_exists($authenticationNameSpace)){

        }
        $this->authenticationNameSpace = $authenticationNameSpace;
        $this->auth = new Auth();
        $this->prepare();
    }

    private function prepare(){
        $namespace = $this->authenticationNameSpace;
        $instance = new $namespace();
        if(!($instance instanceof IAuthentication)){

        }
        $instance->getAuth($this->auth);
    }

    public function getAuth():Auth{
        return $this->auth;
    }

    public function authenticate(Route $route):bool{
        if(!$route->getScope()){
            return true;
        }

        return in_array($route->getScope(),$this->auth->getScopes());

    }


    
}