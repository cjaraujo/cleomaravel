<?php

namespace System\Server\Entities;

class Auth{

    private array $scopes = [];

    private string $name = "";

    private int $id = 0;


    public function setId(int $id):self{
        $this->id = $id;
        return $this;
    }

    public function setName(string $name):self{
        $this->name = $name;
        return $this;
    }

    public function setScopes(array $scopes):self{
        $this->scopes = $scopes;
        return $this;
    }

    public function getId():int{
        return $this->id;
    }

    public function getName():string{
        return $this->name;
    }

    public function getScopes():array{
        return $this->scopes;
    }

}