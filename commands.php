<?php

require "vendor/autoload.php";
require "config.php";

use System\Migration\Migration;
use System\Tools\Time;

Time::updateDateTime();
$action = $argv[1];
excecuteAction($action);

function excecuteAction($action){
    switch($action){
        case 'migrate':
            migrate();
    }
}

function migrate(){
    (new Migration)->update();
}