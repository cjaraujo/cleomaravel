<?php
namespace App\Localisation\Migration;

use App\Localisation\Models\Localisation;
use System\Migration\Interfaces\IMigration;

class M20210125093500PopulateLocalisation implements IMigration{
    public function up():string{
       $localisation = Localisation::instance();
       $localisation->code = 'pt_BR';
       $localisation->image = 'public/localisation/images/pt_BR.png';
       $localisation->insert();

       $localisation = Localisation::instance();
       $localisation->code = 'en_US';
       $localisation->image = 'public/localisation/images/en_US.png';
       $localisation->insert();

       return "Localisation polulated";
    }
    public function down():string{
        return "teste migracao";
    }
}