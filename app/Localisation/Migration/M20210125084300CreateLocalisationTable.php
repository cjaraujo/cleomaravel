<?php
namespace App\Localisation\Migration;

use System\DataBase\TableFactory;
use System\Migration\Interfaces\IMigration;

class M20210125084300CreateLocalisationTable implements IMigration{

    public function up():string{
        $table = TableFactory::mysqli('localisation');
        $table->addInt("id")->size(11)->autoincrement(true);
        $table->addVarchar("code")->size(32)->nullable(false);
        $table->addTinyint("active")->size(1)->defaultValue(1);
        $table->addVarchar("image")->size(64)->nullable(false);
        $table->primaryKey("id");
        $table->uniqueKey("code");
        $table->index("active");
        $table->create();


        return "Localisation table created";
    }
    public function down():string{
        return "teste migracao";
    }
}