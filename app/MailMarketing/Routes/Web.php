<?php

namespace App\MailMarketing\Routes;

use App\MailMarketing\Web\Leads;
use App\MailMarketing\Web\Links;
use System\Server\Interfaces\IRoutes;
use System\Server\Routes;

class Web implements IRoutes{
    public function load(Routes $routes){
        $routes->get('list',Links::class,'table');
        $routes->get('add',Links::class,'formAdd');
        $routes->post('add',Links::class,'add');

        $routes->get('leads/list',Leads::class,'table');
    }
}