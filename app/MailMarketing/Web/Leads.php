<?php

namespace App\MailMarketing\Web;

use App\Layout\Rules\Components\Buttons\Button;
use App\Layout\Rules\Components\Table\Table;
use App\Layout\Rules\Dashboard\Dashboard;
use App\MailMarketing\Models\Leads as ModelsLeads;
use App\MailMarketing\Models\Links;
use System\Server\Entities\Request;

class Leads{
    public function table(Request $request){
        $dashboard = new Dashboard();
        $table = Table::instance($dashboard);
        $table->setColumns(['id','name','email','code','created_at']);

        $filter = $table->filter(url()->toRoute('mailMarketing/leads/list'));
        $filter->input('name');
        $filter->input('email');
        $filter->select('link_id')->addOption('','--Selecione--')->addOptionsFromModels(Links::instance()->result(),'id','name');
        $filter->loadDataArray($request->gets());

        $table->paginate(ModelsLeads::instance()->filter($request->gets()),url()->toRoute('mailMarketing/list'),(int)($request->get('page')),10,$request->gets());
        $table->addButton(Button::instance($dashboard,'add')->addAttr('onclick','window.location.href=\''.url()->toRoute('mailMarketing/add')."'")->addAttr('type','button'));
        $dashboard->setContents( $table->html());

        response()->html($dashboard->html());
    }
}