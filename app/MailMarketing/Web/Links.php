<?php

namespace App\MailMarketing\Web;

use App\Layout\Rules\Components\Buttons\Button;
use App\Layout\Rules\Components\Form\AjaxForm;
use App\Layout\Rules\Components\Table\Table;
use App\Layout\Rules\Dashboard\Dashboard;
use App\MailMarketing\Models\Links as ModelsLinks;
use System\Exceptions\ValidationException;
use System\Server\Entities\Request;
use System\Tools\Time;

class Links{
    public function table(Request $request){
        $dashboard = new Dashboard();
        $table = Table::instance($dashboard);
        $table->setColumns(['id','name','code','created_at']);

        $table->paginate(ModelsLinks::instance()->filter($request->gets()),url()->toRoute('mailMarketing/list'),(int)($request->get('page')),10,$request->gets());
        $table->addButton(Button::instance($dashboard,'add')->addAttr('onclick','window.location.href=\''.url()->toRoute('mailMarketing/add')."'")->addAttr('type','button'));
        $dashboard->setContents( $table->html());

        response()->html($dashboard->html());
    }

    public function formAdd(){
        $dashboard = new Dashboard();

        $form = AjaxForm::instance($dashboard,url()->toRoute('mailMarketing/add'));
        $form->input('name');
        $form->button('save');
        $dashboard->setContents( $form->html());

        response()->html($dashboard->html());
    }

    public function add(Request $request){
        $links = ModelsLinks::instance();
        $links->name = $request->post('name');
        $links->config = '[]';
        $links->code = uniqid('form');
        $links->created_at = Time::getDateTime()->format('Y-m-d H:i:s');

        try{
            $links->validate();
        }catch(ValidationException $e){
            response()->json(['errors'=>$e->getErrors()],422);
        }

        $links->insert();
        response()->json(['success'=>true]);

    }
}