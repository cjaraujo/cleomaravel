<?php

namespace App\MailMarketing\Models;

use System\Models\TableModel;

class Leads extends TableModel{
    protected string $table = "leads";
    protected array $fields = ["id","name","email","link_id","insert_at"];
    protected array $primaryKeys = ["id"];
}