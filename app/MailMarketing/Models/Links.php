<?php

namespace App\MailMarketing\Models;

use System\Models\TableModel;
use System\Models\Validation;
use System\Models\Validations\Required;

class Links extends TableModel{
    protected string $table = "links";
    protected array $fields = ["id","name","config","code","created_at"];
    protected array $primaryKeys = ["id"];

    public function validate(){
        $validation = new Validation();

        $validation->addValidation('name',Required::class);

        $validation->validate($this);
    }
}