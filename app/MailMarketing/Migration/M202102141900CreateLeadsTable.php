<?php

namespace App\MailMarketing\Migration;

use System\DataBase\TableFactory;
use System\Migration\Interfaces\IMigration;

class M202102141900CreateLeadsTable implements IMigration{
    public function up():string{
        $table = TableFactory::mysqli('leads');
        $table->addInt('id')->autoincrement();
        $table->addVarchar('name')->size(128);
        $table->addVarchar('email')->size(128);
        $table->addInt('link_id');
        $table->addDatetime('insert_at');

        $table->primaryKey('id');
        $table->foreignKey('link_id','links','id');
        $table->create();
        return 'Leads table created';
    
    }

    public function down():string{
        return '';
    }
}