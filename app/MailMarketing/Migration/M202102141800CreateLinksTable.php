<?php

namespace App\MailMarketing\Migration;

use System\DataBase\TableFactory;
use System\Migration\Interfaces\IMigration;

class M202102141800CreateLinksTable implements IMigration{
    public function up():string{

        $table = TableFactory::mysqli('links');
        $table->addInt('id')->autoincrement();
        $table->addVarchar('name')->size(128);
        $table->addJson('config');
        $table->addVarchar('code')->size('32');
        $table->addDatetime('created_at');

        $table->primaryKey('id');
        $table->uniqueKey('code');
        $table->create();
        return 'Links table created';

    }

    public function down():string{
        return '';
    }
}