<?php
namespace App\Person\Migration;

use System\DataBase\TableFactory;
use System\Migration\Interfaces\IMigration;

class M20210210083800CreateIndustryTable implements IMigration{
    public function up():string{
        $table = TableFactory::mysqli('industry');
        $table->addInt('id')->size(11)->autoincrement();
        $table->addVarchar('description')->size('128');
        $table->primaryKey('id');
        $table->create();
        return 'insdustry table create';
    }

    public function down():string{
        return '';
    }
}