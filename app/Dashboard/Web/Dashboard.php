<?php
namespace App\Dashboard\Web;

use App\Layout\Rules\Components\Form\AjaxForm;
use App\Layout\Rules\Components\Form\Form;
use App\Layout\Rules\Components\Inputs\Factory;
use App\Layout\Rules\Components\Table\Table;
use App\Layout\Rules\Dashboard\Dashboard as DashboardDashboard;
use App\Users\Models\Users;
use System\Server\Entities\Request;

class Dashboard{
    public function index(Request $request){

        
        $dashboard = new DashboardDashboard();

        $table = Table::instance($dashboard);

        $table->setColumns(['email','name','scope']);
        $table->setData(Users::instance()->result());
        $table->addLink('default','fa fa-user',function($user){
            return url()->toRoute("dashboard/teste/{$user->id}");
        });
        $table->addLink('default','fas fa-search',function($user){
            return url()->toRoute("dashboard/teste/{$user->id}");
        });
        $dashboard->setContents( $table->html());

        response()->html($dashboard->html());
    }

    public function teste(Request $request, int $id){
        $dashboard = new DashboardDashboard();

        $form = AjaxForm::instance($dashboard,'teste');
        $form->input('name');
        $form->input('email');
        $form->input('scope');
        $form->button('save');
        $form->loadData(Users::instance()->getById($id));
        $dashboard->setContents( $form->html());

        response()->html($dashboard->html());
    }
}