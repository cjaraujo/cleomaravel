<?php
namespace App\Dashboard\Routes;

use App\Dashboard\Web\Dashboard;
use System\Server\Interfaces\IRoutes;
use System\Server\Routes;

class Web implements IRoutes{
    public function load(Routes $routes){
        $routes->get("/",Dashboard::class,'index');
        $routes->get("teste/{id}",Dashboard::class,'teste');
    }
}