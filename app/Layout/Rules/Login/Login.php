<?php
namespace App\Layout\Rules\Login;

use App\Layout\Rules\ALayout;
use App\Layout\Rules\Login\Themes\Clear;
use App\Localisation\Rules\Localisation;

class Login extends ALayout{

    private string $urlLogin = "";

    private string $urlRedirect = "";

    private array $localisation = [];
    public function __construct()
    {
        $this->loadTheme();
    }
    public function html(){
        $dictionary = Localisation::instance()->getDictionary();
        $dictionary->loadFile('login');
        return view($this->template,[
            'css'=>$this->css,
            'js'=>$this->js,
            'title'=>$this->title,
            'urlLogin'=>$this->urlLogin,
            'urlRedirect'=>$this->urlRedirect,
            'dictionary'=>$dictionary,
            'localisations'=>$this->localisation]);
    }

    private function loadTheme(){
        (new Clear)->prepare($this);
    }

    public function setUrlLogin(string $urlLogin):self{
        $this->urlLogin = $urlLogin;
        return $this;
    }
    public function setUrlRedirect(string $urlRedirect):self{
        $this->urlRedirect = $urlRedirect;
        return $this;
    }

    public function setLocalisations(array $localisations):self{
        $this->localisation = $localisations;
        return $this;
    }
}