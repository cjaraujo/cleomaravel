<?php

namespace App\Layout\Rules\Login\Themes;

use App\Layout\Rules\ALayout;
use App\Layout\Rules\ATheme;

class Clear extends ATheme{
    function prepare(ALayout $layout){
        $layout->addCss(url()->toRoute('public/common/css/app.css'));

        $layout->addJs(url()->toRoute('public/common/js/app.js'));
        $layout->addJs(url()->toRoute('public/common/js/common.js'));
        $layout->addJs(url()->toRoute('public/common/js/charts.js'));
        $layout->addJs(url()->toRoute('public/common/js/jquery-3.5.1.min.js'));

        $layout->setTitle('Login');

        $layout->setTemplate(url()->toPath('public/layout/template/clear/login.php'));
    }
}