<?php
namespace App\Layout\Rules;

abstract class ATheme{
    protected string $template;

    abstract function prepare(ALayout $layout);

    
}