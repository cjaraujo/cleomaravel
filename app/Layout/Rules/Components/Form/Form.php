<?php
namespace App\Layout\Rules\Components\Form;

use App\Layout\Rules\AComponent;
use App\Layout\Rules\ALayout;
use App\Layout\Rules\Components\Buttons\Button;
use App\Layout\Rules\Components\Inputs\ACheckbox;
use App\Layout\Rules\Components\Inputs\AInput;
use App\Layout\Rules\Components\Inputs\ASelect;
use App\Layout\Rules\Components\Inputs\Factory;
use App\Layout\Rules\Components\Inputs\Themes\Clear\Input;
use System\Models\AModel;

class Form  extends AComponent{
    protected string $name = '';
    protected array $inputs = [];

    protected array $buttons = [];

    protected array $sizes = [];

    protected Factory $factory;

    protected string $action = '';

    protected string $method = '';

    protected string $theme = 'Clear';
    protected string $id = '';


    public function __construct(ALayout $layout,string $action,string $method = 'POST',string $theme = 'Clear')
    {
        parent::__construct($layout);

        $this->factory = Factory::instance($layout);
        $this->action = $action;
        $this->method = $method;
        $this->id = uniqid('form_');

        $this->addAttr('action',$this->action);
        $this->addAttr('method',$this->method);
        $this->addAttr('id',$this->id);
        
        $this->theme = $theme;
    }
    function html():string{
        return (new Themes\Clear())->html($this);
    }
    public function setName(string $name):self{
        $this->name = $name;
        return $this;
    }
    public function getName():string{
        return $this->name;
    }
    function addButton(Button $button):self{
        $this->buttons[] = $button;
        return $this;
    }
    function loadData(AModel $model){
        foreach($model->getFields() as $field){
            if(!isset($this->inputs[$field])){
                continue;
            }
            $this->inputs[$field]->setValue($model->{$field});
        }
    }
    function loadDataArray(array $data):self{
        foreach($this->inputs as $name=>$input){
            if(isset($data[$name])){
                $input->setValue($data[$name]);
            }
        }
        return $this;
    }

    function addInput(AInput $input, int $size = 6):self{
        $this->inputs[$input->getName()] = $input;
        $this->sizes[$input->getName()] = $size;
        return $this;
    }

    function getComponents():array{
        return $this->inputs;
    }
    function getSizes():array{
        return $this->sizes;
    }
    function getButtons():array{
        return $this->buttons;
    }

    function input(string $name, int $size = 6):AInput{
        $input = $this->factory->input($name);
        $this->addInput($input,$size);
        return $input;
    }
    function select(string $name, int $size = 6):ASelect{
        $select = $this->factory->select($name);
        $this->addInput($select,$size);
        return $select;
    }

    function checkbox(string $name, int $size = 6):ACheckbox{
        $checkbox = $this->factory->checkbox($name);
        $this->addInput($checkbox,$size);
        return $checkbox;
    }

    function button(string $name, string $type = Button::BUTTON_DEFAULT):Button{
        $button = new Button($this->layout,$name,$type);
        $this->addButton($button);
        return $button;
    }

    public static function instance(ALayout $layout,string $action,string $method = 'POST',string $theme = 'Clear'):Form{
        return new Form($layout,$action,$method,$theme);
    }
    
    public function getId():string{
        return $this->id;
    }
}