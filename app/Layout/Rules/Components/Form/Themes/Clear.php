<?php

namespace App\Layout\Rules\Components\Form\Themes;

use App\Layout\Rules\Components\Form\Form;

class Clear{
    public function html(Form $form):string{
      $attr = '';
        foreach($form->getAttr() as $name=>$value){
            $attr .= "$name=\"$value\" ";
        }
        $html = "
        <div class='col-12 alert'></div>
        <form class=\"card card-default\" $attr>
       
        <div class=\"card-header\">
          <h3 class=\"card-title\">{$form->getLayout()->getDictionary()->get($form->getName())}</h3>
          <div class=\"card-tools\">
          </div>
        </div>
        <div class=\"card-body\"><div class=\"row\"> ";
        
        foreach($form->getComponents() as $name=>$input){
            $html .="<div class=\"col-md-{$form->getSizes()[$name]}\">";
            $html .= $input->html();
            $html .="</div>";
        }
        $html .="</div></div>
        <div class=\"card-footer\">";
        foreach($form->getButtons() as $button){
          $html .= $button->html();
        }
        $html .="</form>
      </div>";
      return $html;
    }
}