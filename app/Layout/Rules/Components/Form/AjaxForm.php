<?php
namespace App\Layout\Rules\Components\Form;

use App\Layout\Rules\AComponent;
use App\Layout\Rules\ALayout;
use App\Layout\Rules\Components\Buttons\Button;
use App\Layout\Rules\Components\Inputs\AInput;
use App\Layout\Rules\Components\Inputs\ASelect;
use App\Layout\Rules\Components\Inputs\Factory;
use App\Layout\Rules\Components\Inputs\Themes\Clear\Input;
use System\Models\AModel;

class AjaxForm  extends Form{

    
    public function __construct(ALayout $layout,string $action,string $method = 'POST',string $theme = 'Clear')
    {
        parent::__construct($layout,$action,$method,$theme);
        
        $this->createScript();
        
    }

    private function createScript(){
        $this->getLayout()->addScript(obfuscateJs("
            $(function(){
                $('#{$this->id}').submit(function(e) {
                    
                    e.preventDefault();
                    $.ajax({
                        url: '{$this->action}',
                        type: '{$this->method}',
                        data: $('#{$this->id}').serialize(),
                        cache: false,
                        success: function(data) {
                            $('.alert').html(
                                    \"<div class='alert alert-success alert-dismissible'> \"
                                        + \"<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button> \"
                                        + \"<h5><i class='icon fas fa-check'></i> Sucesso!</h5> \"
                                        + \"Salvo com sucesso. \"
                                    + \"</div> \"
                            )
                        },
                        error: function (data) {
                            data = data.responseJSON;
                            $.each(data.errors, function(i, erro) {
                                $('[name='+i+']').addClass('is-invalid');
                                $('[name='+i+']').parent().append(\"<span id='exampleInputEmail1-error' class='error invalid-feedback'>\"+erro+\"</span>\");
                            });
                        }
                    });
                });
                $('input, select').on('change',function(){\$(this).removeClass('is-invalid');\$(this).parent().find('.error').remove()});
            });
        
        "));
    }
    public static function instance(ALayout $layout,string $action,string $method = 'POST',string $theme = 'Clear'):AjaxForm{
        return new AjaxForm($layout,$action,$method,$theme);
    }

}