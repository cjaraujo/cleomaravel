<?php

namespace App\Layout\Rules\Components\Table;

use App\Layout\Rules\AComponent;
use App\Layout\Rules\ALayout;
use App\Layout\Rules\Components\Buttons\Button;
use App\Layout\Rules\Components\Form\Form;
use App\Layout\Rules\Components\Table\Themes\Clear;
use PHPUnit\Framework\Constraint\LogicalAnd;
use System\Models\TableModel;

class Table extends AComponent{

    private array $columns = [];

    private array $data = [];

    private array $links = [];

    private array $dataCallbacks = [];

    private array $buttons = [];

    private Paginate $paginate;

    private Form $filter;

    private bool $hasFilter = false;
    public function __construct(ALayout $layout)
    {
        parent::__construct($layout);
        $this->paginate = new Paginate();
        
    }

    public function addButton(Button $button){
        $this->buttons[] = $button;
    }
    public function setColumns(array $columns):self{
        $this->columns = $columns;
        return $this;
    }

    public function setData(array $data):self{
        $this->data = $data;
        return $this;
    }

    public function filter(string $url):Form{
        $this->hasFilter = true;
        $this->filter = Form::instance($this->layout,$url,'GET');
        $this->filter->setName('filters');
        $this->filter->addButton(Button::instance($this->layout,'search',Button::BUTTON_SUCCESS));
        return $this->filter;
    }

    public function getData():array{
        return $this->data;
    }

    public function getColumns():array{
        return $this->columns;
    }

    public function addLink(string $type,string $icon, \Closure $link):self{
        $this->links[] = ['type'=>$type,'icon'=>$icon,'link'=>$link];
        return $this;
    }

    public function getLinks():array{
        return $this->links;
    }

    function html():string{
        return (new Clear)->html($this);
    }

    function addCallback(string $name, \Closure $callback):self{
        $this->dataCallbacks[$name] = $callback;
        return $this;
    }

    public function getCallbacks():array{
        return $this->dataCallbacks;
    }

    public static function instance(ALayout $layout):Table{
        return new Table($layout);
    }
    public function getButtons():array{
        return $this->buttons;
    }

    public function paginate(TableModel $model,string $url,int $page,int $perPage = 20,array $uri = []){
        $this->paginate->create($model,$url,$page,$perPage,$uri);
        $this->data = $this->paginate->getData();
    }

    public function getPagination():Paginate{
        return $this->paginate;
    }

    public function hasFilter():bool{
        return $this->hasFilter;
    }

    public function getFilter():Form{
        return $this->filter;
    }

}