<?php
namespace App\Layout\Rules\Components\Table\Themes;

use App\Layout\Rules\Components\Table\Table;

class Clear{
    public function html(Table $table):string{


        $html = '';
        if($table->hasFilter()){
          $html .= $table->getFilter()->addAttr('style','display:none')->html();
          $table->getLayout()->addScript('
              $("#filter").on("click",function(){
                  if($("#'.$table->getFilter()->getId().'").css("display")=="none"){
                    $("#'.$table->getFilter()->getId().'").show(250);
                    return;
                  }
                  $("#'.$table->getFilter()->getId().'").hide(250);
              });
          ');
        }
        $html .=  '<div class="card">
        <div class="card-header">
        
          <div class="card-tools" style="float:left">
            
            ';

        foreach($table->getButtons() as $button){
          $html .= $button->html();
          
        }

        $html .='
          
          </div>
          <div class="card-tools" style="float:right">';
          if($table->hasFilter()){
            $html .= '<button type="button" class="btn btn-success" id="filter">Filtros</button>';
          }
          $html .= '</div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
          <table class="table table-head-fixed text-nowrap">
            <thead>
              <tr>';
              if($table->getLinks()){
                $html .= "<th></th>";
              }
              foreach($table->getColumns() as $column){
                $html .= "<th>{$table->getLayout()->getDictionary()->get($column)}</th>";
              }
             
              $html .='</tr>
            </thead>
            <tbody>';
            foreach($table->getData() as $row){
                $html .= '<tr>';
                if($table->getLinks()){
                  $html .= "<td>";
                  foreach($table->getLinks() as $link){
                      $html .= "<a class='btn ".$link['type']." btn-sm' href='{$link['link']($row)}'><i class='{$link['icon']}'></i></a>";
                  }
                  $html .= "</td>";
                }
                foreach($table->getColumns() as $column){
                  if(isset($table->getCallbacks()[$column])){
                    $html .= "<td>{$table->getCallbacks()[$column]($row->{$column})}</td>";
                    continue;
                  }
                    $html .= "<td>{$row->{$column}}</td>";
                }
                
                $html .= '</tr>';
              }
              
             
              $html .='</tbody>
          </table>
        
        <div class="card-footer clearfix"><div class="row"><div class="col-sm-12 col-md-5"><div class="dataTables_info" id="example2_info" role="status" aria-live="polite">Showing '.$table->getPagination()->getPage()*$table->getPagination()->getPerPage().' to '.(($table->getPagination()->getPage()*$table->getPagination()->getPerPage())+$table->getPagination()->getPerPage()).' of '.$table->getPagination()->getCount().' entries</div></div><div class="col-sm-12 col-md-7"><div class="pagination pagination-md m-0 float-right" id="example2_paginate"><ul class="pagination"><li class="paginate_button page-item previous disabled" id="example2_previous"><a href="#" aria-controls="example2" data-dt-idx="0" tabindex="0" class="page-link">Previous</a></li>';
        foreach($table->getPagination()->getLinks() as $link){
          $active = $link['page'] == $table->getPagination()->getPage()?'active':'';
          $html .= '<li class="paginate_button page-item '.$active.'"><a href="'.$link['link'].'" aria-controls="example2" data-dt-idx="1" tabindex="0" class="page-link">'.($link['page']+1).'</a></li>';
        
        }
   
        $html .='<li class="paginate_button page-item next" id="example2_next"><a href="#" aria-controls="example2" data-dt-idx="7" tabindex="0" class="page-link">Next</a></li></ul></div></div></div>
        <!-- /.card-body -->
      </div></div></div>';
      return $html;
    }
}