<?php
namespace App\Layout\Rules\Components\Inputs;

use App\Layout\Rules\AComponent;
use App\Layout\Rules\ALayout;

abstract class ACheckbox extends AInput{

    protected array $values = [];

    protected array $fields = [];

    public function setValue(string $value):self{
        $this->values[] = $value;
        return $this;
    }
    public function setValues(array $values):self{
        $this->values = $values;
        return $this;
    }

    public function setFields(array $fields):self{
        $this->fields = $fields;
        return $this;
    }
}