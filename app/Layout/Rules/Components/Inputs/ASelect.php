<?php
namespace App\Layout\Rules\Components\Inputs;

abstract class ASelect extends AInput{


    protected array $options = [];

    public function addOption(string $value,$name):self{
        $this->options[$value] = $name;
        return $this;
    }

    public function addOptionsFromModels(array $models,string $key,string $name):self{
        foreach($models as $model){
            $this->options[$model->{$key}] = $model->{$name};
        }
        return $this;
    }

    public function getOptions():array{
        return $this->options;
    }

}