<?php

namespace App\Layout\Rules\Components\Inputs\Themes\Clear;

use App\Layout\Rules\Components\Inputs\AInput;

class Input extends AInput{
    public function html():string{
        $attr = '';

        foreach($this->attrs as $name=>$value){
            $attr .= "$name=\"$value\" ";
        }
        return "<div class=\"form-group\">
                    <label>{$this->layout->getDictionary()->get($this->name)}</label>
                    <input type=\"text\" class=\"form-control\" {$attr}placeholder=\"{$this->layout->getDictionary()->get($this->name)}\">
                </div>";
    }
}