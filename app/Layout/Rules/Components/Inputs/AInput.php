<?php
namespace App\Layout\Rules\Components\Inputs;

use App\Layout\Rules\AComponent;
use App\Layout\Rules\ALayout;

abstract class AInput extends AComponent{
    protected string $name = "";

    public function __construct(ALayout $layout, string $name)
    {
        parent::__construct($layout);
        $this->name = $name;
        $this->addAttr('name',$name);
    }

    public function setValue(string $value):self{
        $this->addAttr("value",$value);
        return $this;
    }

    public function getName():string{
        return $this->name;
    }

    
}