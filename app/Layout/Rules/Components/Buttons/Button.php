<?php

namespace App\Layout\Rules\Components\Buttons;

use App\Layout\Rules\AComponent;
use App\Layout\Rules\ALayout;
use App\Layout\Rules\Components\Buttons\Themes\Clear;

class Button extends AComponent{

    const BUTTON_DEFAULT = 'default';
    const BUTTON_SUCCESS = 'success';
    const BUTTON_DANGER = 'danger';
    private string $name;

    private string $type;
    public function __construct(ALayout $layout, string $name,string $type = self::BUTTON_DEFAULT)
    {
        parent::__construct($layout);
        $this->name = $name;
        $this->type = $type;
    }

    function html():string{

        return (new Clear)->html($this);
    }

    public function getName():string{
        return $this->name;
    }

    public function getType():string{
        return $this->type;
    }

    public static function instance(ALayout $layout, string $name,string $type = self::BUTTON_DEFAULT):Button{
        return new Button($layout,$name,$type);
    }
}