<?php

namespace App\Layout\Rules\Components\Buttons\Themes;

use App\Layout\Rules\Components\Buttons\Button;

class Clear{
    public function html(Button $button){
        $attr = '';
        foreach($button->getAttr() as $name=>$value){
            $attr .= "$name=\"$value\" ";
        }
        return "<button class=\"{$this->getClass($button->getType())} pull-right\" $attr>{$button->getLayout()->getDictionary()->get($button->getName())}</button>";
    }

    private function getClass(string $type){
        switch($type){
            case Button::BUTTON_DEFAULT: 
                return "btn btn-default";
            case Button::BUTTON_SUCCESS:
                return "btn btn-success";
            case Button::BUTTON_DANGER:
                return "btn btn-danger";
        }
    }
}