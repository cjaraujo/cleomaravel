<?php

namespace App\Layout\Rules;

abstract class AComponent{

    protected ALayout $layout;


    protected array $attrs = [];


    public function __construct(ALayout $layout)
    {
        $this->layout = $layout;
    }

    public function addAttr(string $name, string $value):self{
        $this->attrs[$name] = $value;
        return $this;
    }

    abstract function html():string;

    public function getLayout():ALayout{
        return $this->layout;
    }

    public function getAttr():array{
        return $this->attrs;
    }

}