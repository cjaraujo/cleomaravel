<?php

namespace App\Layout\Rules\Dashboard;

use App\Layout\Rules\ATheme as LayoutsATheme;
use App\Localisation\Rules\Dictionary;

abstract class AThemeDashboard extends LayoutsATheme{


    abstract function buildMenu(Menu $dashboard,Dictionary $dictionary):string;
}