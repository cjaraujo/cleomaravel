<?php

namespace App\Layout\Rules\Dashboard;

use App\Layout\Rules\ALayout;
use App\Layout\Rules\Dashboard\Themes\Clear;
use App\Localisation\Rules\Localisation;

class Dashboard extends ALayout{

    private array $breadcrumb = [];
    private Menu $menu;
    private AThemeDashboard $theme;
    private string $contents = '';
    public function __construct()
    {
        $this->theme = new Clear();
        $this->menu = new Menu();
        $this->loadTheme();
        parent::__construct();
        $this->dictionary->loadFile('login');
        $this->dictionary->loadFile('dashboard_menu');
    }
    public function html(){
       
        return view($this->template,[
            'css'=>$this->css,
            'js'=>$this->js,
            'title'=>$this->title,
            'menu' =>$this->theme->buildMenu($this->menu,$this->getDictionary()),
            'contents'=>$this->contents,
            'scripts'=>$this->scripts,
            'dictionary'=>$this->dictionary]);
    }

    public function setContents(string $contents):self{
        $this->contents = $contents;
        return $this;
    }

    private function loadTheme(){
        $this->theme->prepare($this);
    }

}