<?php

namespace App\Layout\Rules\Dashboard\Themes;

use App\Layout\Rules\ALayout;
use App\Layout\Rules\Dashboard\AThemeDashboard;
use App\Layout\Rules\Dashboard\Menu;
use App\Localisation\Rules\Dictionary;

class Clear extends AThemeDashboard{
    function prepare(ALayout $layout){
        $layout->addJs(url()->toRoute('public/common/js/jquery.min.js'));
        $layout->addJs(url()->toRoute('public/common/js/bootstrap.bundle.min.js'));

        $layout->addJs(url()->toRoute('public/common/js/adminlte.min.js'));
        $layout->addJs(url()->toRoute('public/common/js/demo.js'));



        $layout->addCss(url()->toRoute('public/common/css/all.min.css'));
        $layout->addCss(url()->toRoute('public/common/css/adminlte.min.css'));
        $layout->addCss(url()->toRoute('public/common/css/ionicons.min.css'));

        $layout->setTemplate(url()->toPath('public/layout/template/clear/dashboard.php'));
    }

    public function buildMenu(Menu $menu,Dictionary $dictionary):string{
        $menus = $menu->getMenus();

        $html = "<ul class=\"nav nav-pills nav-sidebar flex-column\" data-widget=\"treeview\" role=\"menu\" data-accordion=\"false\">";
        $html .= $this->buildMenuRecursive($menu->getMenus(),$dictionary);
        $html .= "</ul>";
        return $html;
      
    }

    private function buildMenuRecursive(array $menus,Dictionary $dictionary):string{
        $html = "";
        foreach($menus as $menu){
            $menuHtml = "";
            if(count($menu->getChildren())){
                $menuHtml .= "<li class=\"nav-item has-treeview\"><a href=\"javascript:void(0)\" class=\"nav-link\">
                <i class=\"nav-icon fas fa-th\"></i>
                <p>{$dictionary->get($menu->name)} <i class=\"right fas fa-angle-left\"></i></p>";
            }else{
                $menuHtml .= "<li class=\"nav-item\"><a href=\"{$menu->url}\" class=\"nav-link\">
                <i class=\"nav-icon fas fa-th\"></i>
                <p>{$dictionary->get($menu->name)}</p>";
            }
            $menuHtml .= "</a>";
            if(count($menu->getChildren())){
                $menuHtml .= "<ul class=\"nav nav-treeview\" style=\"display: none;\">";
                $menuHtml .= $this->buildMenuRecursive($menu->getChildren(),$dictionary);
                $menuHtml .= "</ul>";
            }
            $menuHtml .= "</li>";
            $html .= $menuHtml;
        }
        return $html;
    }
}