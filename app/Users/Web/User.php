<?php
namespace App\Users\Web;

use App\Export\Rules\Table\Csv;
use App\Layout\Rules\Components\Buttons\Button;
use App\Layout\Rules\Components\Form\AjaxForm;
use App\Layout\Rules\Components\Table\Table;
use App\Layout\Rules\Dashboard\Dashboard;
use App\Users\Models\Users;
use App\Users\Models\UsersGroups;
use System\Exceptions\ValidationException;
use System\Server\Entities\Request;

class User{

    public function table(Request $request){

        $dashboard = new Dashboard();
        $dashboard->getDictionary()->loadFile('users');
        //Instanciando tabela
        $table = Table::instance($dashboard);
        $table->setColumns(['id','email','name','id_group']);

        //criando filtros
        $filter = $table->filter(url()->toRoute('users/list'));
        $filter->input('name');
        $filter->input('email');
        $filter->select('id_group')->addOption('','--Selecione--')->addOptionsFromModels(UsersGroups::instance()->result(),'id','name');
        $filter->loadDataArray($request->gets());

        //Aplicando paginação e filtros
        $table->paginate(Users::instance()->filter($request->gets()),url()->toRoute('users/list'),(int)($request->get('page')),10,$request->gets());

        //adicionando links laterais
        $table->addLink('btn-info','fas fa-pencil-alt',function($user){
            return url()->toRoute("users/form/{$user->id}");
        });
        $table->addLink('btn-danger','fas fa-trash',function($user){
            return url()->toRoute("users/delete/{$user->id}");
        });

        //Adicionando callback na coluna (substitui o dado)
        $table->addCallback('id_group',function($column){
            return UsersGroups::instance()->getById($column)->name;
        });
        //adicionando Botões
        $table->addButton(Button::instance($dashboard,'add')->addAttr('onclick','window.location.href=\''.url()->toRoute('users/form')."'")->addAttr('type','button'));
        $table->addButton(Button::instance($dashboard,'grudo de usuario')->addAttr('onclick','window.location.href=\''.url()->toRoute('users/users-groups/list')."'")->addAttr('type','button'));
        $table->addButton(Button::instance($dashboard,'export')->addAttr('onclick','window.location.href=\''.url()->toRoute('users/export').'?'.http_build_query($request->gets())."'")->addAttr('type','button'));
        $dashboard->setContents( $table->html());

        response()->html($dashboard->html());
    }

    public function formAdd(){
        $dashboard = new Dashboard();

        $form = AjaxForm::instance($dashboard,url()->toRoute('users/add'));
        $form->input('name');
        $form->input('email');
        $form->input('password');
        $form->select('id_group')->addOptionsFromModels(UsersGroups::instance()->result(),'id','name');
        $form->button('save');
        $form->button('concluir');
        $dashboard->setContents( $form->html());

        response()->html($dashboard->html());
    }

    
    public function formUpdate(Request $request, $id){
        $dashboard = new Dashboard();

        $form = AjaxForm::instance($dashboard,url()->toRoute('users/update/'.$id));
        $form->input('name');
        $form->input('email');
        $form->input('password');
        $form->select('id_group')->addOptionsFromModels(UsersGroups::instance()->result(),'id','name');
        $form->button('save');
        $form->loadData(Users::instance()->getById($id));
        $form->button('concluir',Button::BUTTON_DANGER)->addAttr('disable','true');
        $dashboard->setContents( $form->html());

        response()->html($dashboard->html());
    }

    public function add(Request $request){

        $user = Users::instance();
        $user->setValues($request->posts());

        try{
            $user->validate();
        }catch(ValidationException $e){
            response()->json(['errors'=>$e->getErrors()],422);
        }

        $user->insert();
        response()->json(['success'=>true]);
    }

    public function update(Request $request, $id){
        $user = Users::instance()->getById($id);
        $user->setValues($request->posts());
        $user->save();
        response()->json(['success'=>true]);
    }

    public function exportList(Request $request){
        $path = url()->toPath('public/export/user.csv');
        $csv = Csv::instance(['id','email','name','id_group'],Users::instance()->filter($request->gets())->result());
        $csv->export($path);
        response()->redirect(url()->toRoute('public/export/user.csv'));
    }
}