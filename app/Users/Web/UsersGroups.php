<?php
namespace App\Users\Web;

use App\Layout\Rules\Components\Buttons\Button;
use App\Layout\Rules\Components\Form\AjaxForm;
use App\Layout\Rules\Components\Table\Table;
use App\Layout\Rules\Dashboard\Dashboard;
use App\Users\Models\Users;
use App\Users\Models\UsersGroups as ModelsUsersGroups;
use App\Users\Rules\Scopes;
use System\Exceptions\ValidationException;
use System\Server\Entities\Request;

class UsersGroups{

    public function table(){
        $dashboard = new Dashboard();

        $table = Table::instance($dashboard);

        $table->setColumns(['id','name']);
        $table->setData(ModelsUsersGroups::instance()->result());
        $table->addLink('default','fas fa-pencil-alt',function($user){
            return url()->toRoute("users/form/{$user->id}");
        });
        $table->addButton(Button::instance($dashboard,'add')->addAttr('onclick','window.location.href=\''.url()->toRoute('users/users-groups/form')."'")->addAttr('type','button'));
        $dashboard->setContents( $table->html());

        response()->html($dashboard->html());
    }

    public function formAdd(){
        $dashboard = new Dashboard();

        $form = AjaxForm::instance($dashboard,url()->toRoute('users/users-groups/add'));
        $form->input('name');
        $form->checkbox('scopes')->setFields(Scopes::getAllScopes());
        $form->button('save');
        $dashboard->setContents( $form->html());

        response()->html($dashboard->html());
    }

    public function add(Request $request){
        $user = ModelsUsersGroups::instance();

        $user->name = $request->post('name');
        $user->prepareScopes((array)($request->post('scopes')));
        try{
            $user->validate();
        }catch(ValidationException $e){
            response()->json(['errors'=>$e->getErrors()],422);
        }
        $user->insert();
        response()->json(['success'=>true]);
    }
}