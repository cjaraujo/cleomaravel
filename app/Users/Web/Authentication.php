<?php
namespace App\Users\Web;

use App\Layout\Rules\Login\Login;
use App\Localisation\Rules\Localisation;
use App\Users\Rules\UserSession;
use System\Server\Entities\Request;

class Authentication{

    public function index(){
        response()->html(
            (new Login)
                ->setUrlLogin(url()->toRoute('users/login'))
                ->setLocalisations(Localisation::instance()->getActives())
                ->setUrlRedirect(url()->toRoute('dashboard'))
                ->html()
        );
    }
    public function login(Request $request){
        $userRules = new UserSession();
        if($userRules->authenticate($request->post('user'),$request->post('password'))){
            response()->json(['success'=>true]);
        }
        response()->json(['success'=>false]);
    }


}