<?php
namespace App\Users\Rules;

use App\Users\Models\Users;
use App\Users\Rules\Authentication\Authentication;

class Scopes{
    const USERS_ADD = 'users_add';
    const USERS_LIST = 'users_list';
    const USERS_GROUPS_ADD = 'users_groups_add';
    const USERS_GROUPS_LIST = 'users_groups_list';

    public static function getAllScopes():array{
        return [
            self::USERS_ADD,
            self::USERS_LIST,
            self::USERS_GROUPS_ADD,
            self::USERS_GROUPS_LIST
        ];
    }
}