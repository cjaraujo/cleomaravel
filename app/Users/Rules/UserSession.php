<?php
namespace App\Users\Rules;

use App\Users\Models\Users;
use App\Users\Rules\Authentication\Authentication;

class UserSession{

    private Users $usersModel;
    public function __construct()
    {
        $this->usersModel = Users::instance();
    }
    public function authenticate($user,$password):bool{
        $userModel = $this->usersModel->getByName($user);

        if(!$userModel->id){
            $userModel = $this->usersModel->getByEmail($user);
        }
        if(!$userModel->id){
            return false;
        }
        if(security()->verify($userModel->password,$password)){
            session()->set(Authentication::USER_SESSION_KEY,$userModel->id);
            return true;
        }
        return false;
    }

    public function getUserInSession():Users{
        if(session()->get(Authentication::USER_SESSION_KEY)){
            return $this->usersModel->getById(session()->get(Authentication::USER_SESSION_KEY));
        }
        return $this->usersModel;
    }

    public static function instance():UserSession{
        return new UserSession();
    }
}