<?php

namespace App\Users\Menus;

use App\Layout\Interfaces\IMenu;
use App\Layout\Rules\Dashboard\Menu;

class Dashboard implements IMenu{
    public function load(Menu $menu){
        $usersMenu = $menu->addMenu();
        $usersMenu->name = 'users';
        $usersMenu->icon = 'fa fa-users';
        
        $usersMenu2 = $usersMenu->addChild();
        $usersMenu2->name = 'users_list';
        $usersMenu2->icon = 'fa fa-users';
        $usersMenu2->url = url()->toRoute('users/list');
        
    }
}