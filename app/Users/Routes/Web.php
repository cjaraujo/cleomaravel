<?php
namespace App\Users\Routes;

use App\Users\Web\Authentication;
use App\Users\Web\User;
use App\Users\Web\UsersGroups;
use System\Server\Interfaces\IRoutes;
use System\Server\Routes;

class Web implements IRoutes{
    public function load(Routes $routes){
        $routes->post('login',Authentication::class,'login');
        $routes->get('/',Authentication::class,'index');


        $routes->get('list',User::class,'table');

        $routes->get('form/{id}',User::class,'formUpdate');
        $routes->get('form',User::class,'formAdd');
        $routes->get('export',User::class,'exportList');
        $routes->post('add',User::class,'add');
        $routes->post('update/{id}',User::class,'update');

        $routes->get('users-groups/list',UsersGroups::class,'table');
        $routes->get('users-groups/form',UsersGroups::class,'formAdd');
        $routes->post('users-groups/add',UsersGroups::class,'add');

    }
    
}