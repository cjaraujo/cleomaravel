<?php
namespace App\Users\Models;

use System\Models\TableModel;
use System\Models\Validation;
use System\Models\Validations\Required;

class UsersGroups extends TableModel{
    protected string $table = "users_groups";
    protected array $fields = ["id","name","scope"];
    protected array $primaryKeys = ["id"];


    public function getById(int $id):self{
        $this->queryFactory->where('id','=',$id);
        return $this->get();
    }
    
    public function prepareScopes(array $scopes){
        $this->scope = implode(' ',$scopes);
    }

    public function validate(){
        $validation = new Validation();

        $validation->addValidation('name',Required::class);
        $validation->addValidation('scope',Required::class);

        $validation->validate($this);
    }
}