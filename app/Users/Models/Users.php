<?php
namespace App\Users\Models;

use System\Models\TableModel;
use System\Models\Validation;
use System\Models\Validations\Required;

class Users extends TableModel{
    protected string $table = "users";
    protected array $fields = ["id","id_group","email","name","password"];
    protected array $primaryKeys = ["id"];

    public function getByName(string $name):self{
        $this->queryFactory->where('name','=',$name);
        return $this->get();
    }

    public function getByEmail(string $email):self{
        $this->queryFactory->where('email','=',$email);
        return $this->get();
    }
    public function getById(int $id):self{
        $this->queryFactory->where('id','=',$id);
        return $this->get();
    }

    public function getGroup():UsersGroups{
        return UsersGroups::instance()->getById($this->id_group);
    }

    public function validate(){
        $validation = new Validation();

        $validation->addValidation('name',Required::class);
        $validation->addValidation('email',Required::class);

        $validation->addValidation('password',Required::class);
        $validation->addValidation('id_group',Required::class);

        $validation->validate($this);
    }
}