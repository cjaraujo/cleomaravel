<?
namespace App\Users\Migration;
use System\Migration\Interfaces\IMigration;
use System\DataBase\TableFactory;

class M20210119070000CreateUsersTable implements IMigration{
    public function up():string{
        $table = TableFactory::mysqli('users');
        $table->addInt('id')->size('11')->autoincrement();
        $table->addInt('id_group')->size('11');
        $table->addVarchar('email')->size('32');
        $table->addVarchar('name')->size('32');
        $table->addVarchar('password')->size('128');

        $table->primaryKey('id');
        $table->uniqueKey('email');
        $table->uniqueKey('name');
        $table->foreignKey('id_group','users_groups','id');
        $table->create();


        return "Users table created";
    }
    public function down():string{
        return "teste migracao";
    }
}