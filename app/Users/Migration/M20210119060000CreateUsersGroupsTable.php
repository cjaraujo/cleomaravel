<?php
namespace App\Users\Migration;
use System\Migration\Interfaces\IMigration;
use System\DataBase\TableFactory;

class M20210119060000CreateUsersGroupsTable implements IMigration{
    public function up():string{
        $table = TableFactory::mysqli('users_groups');
        $table->addInt('id')->size('11')->autoincrement();

        $table->addVarchar('name')->size('32');

        $table->addVarchar('scope')->size('255');

        $table->primaryKey('id');
        $table->uniqueKey('name');
        $table->create();


        return "UsersGroups table created";
    }
    public function down():string{
        return "teste migracao";
    }
}