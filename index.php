<?php
require "vendor/autoload.php";
require "config.php";
require "system/Tools/Functions.php";
use System\Server\Controller;
use System\Tools\Time;

$uri = explode('/',trim($_SERVER['REDIRECT_URL'],'/'));
if($uri && reset($uri) == 'public'){
    include(url()->toPath($_SERVER['REDIRECT_URL']));
    exit;
}
Time::updateDateTime();
$controler = new Controller($_SERVER['REDIRECT_URL'],$_SERVER['REQUEST_METHOD']);
$controler->dispatch();
