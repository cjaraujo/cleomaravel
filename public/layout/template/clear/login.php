<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?= $title?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <link rel="apple-touch-icon" sizes="180x180" href="img/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon-16x16.png">
    <link rel="manifest" href="img/site.webmanifest">
    <link rel="mask-icon" href="img/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <meta name="description" content="Page description">
    <!--Twitter Card data-->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@publisher_handle">
    <meta name="twitter:title" content="Page Title">
    <meta name="twitter:description" content="Page description less than 200 characters">
    <meta name="twitter:creator" content="@author_handle">
    <meta name="twitter:image" content="http://www.example.com/image.jpg">
    <!--Open Graph data-->
    <meta property="og:title" content="Title Here">
    <meta property="og:type" content="article">
    <meta property="og:url" content="http://www.example.com/">
    <meta property="og:image" content="http://example.com/image.jpg">
    <meta property="og:description" content="Description Here">
    <meta property="og:site_name" content="Site Name, i.e. Moz">
    <meta property="fb:admins" content="Facebook numeric ID">
    <?php foreach($css as $link) { ?>
      <link rel="stylesheet" media="all" href="<?= $link?>">
    <?php } ?>
    <script>var viewportmeta = document.querySelector('meta[name="viewport"]');
if (viewportmeta) {
  if (screen.width < 375) {
    var newScale = screen.width / 375;
    viewportmeta.content = 'width=375, minimum-scale=' + newScale + ', maximum-scale=1.0, user-scalable=no, initial-scale=' + newScale + '';
  } else {
    viewportmeta.content = 'width=device-width, maximum-scale=1.0, initial-scale=1.0';
  }
}</script>
<?php foreach($js as $link) { ?>
      <script src="<?= $link?>"></script>
    <?php } ?>
  </head>
  <body>
    <div class="out">
      <div class="login">
        <div class="login__container">
          <div class="login__wrap">
            <div class="login__head"><a class="login__logo" href="#"><img class="login__pic" src="img/logo-white.svg" alt=""></a></div>
            <form class="login__form">
            <div>
              <?php foreach($localisations as $localisation){ ?>
                  <button type="button" code="<?= $localisation->code ?>" class='localisation' style='float:right;margin-right:5px;margin-top:5px' ><img style='width:32px' src="<?= url()->toRoute($localisation->image)?>"></button>
                <?php } ?>
            </div>
              <div class="login__body">
                <div class="login__title login__title_sm"><?= $title?></div>
                <div class="login__field field">
                  <div class="field__wrap"><input class="field__input" id='user' name="user" placeholder="<?=$dictionary->get('user')?>"></div>
                </div>
                <div class="login__field field">
                  <div class="field__wrap"><input class="field__input" id='password' type="password" name="password" placeholder="<?=$dictionary->get('password')?>"></div>
                </div>
                <button class="login__btn btn btn btn_blue" type="button" id="login">Login</button>
                <ul class="login__links">
                  <li><a class="login__link" href="#"><?=$dictionary->get('forgot')?></a></li>
                </ul>
              </div>
            </form>
            <div class="login__bottom">
              <ul class="login__links">
                <li><a class="login__link" href="#"><?=$dictionary->get('terms')?></a></li>
              </ul>
            </div>
          </div>
        </div><label class="switch switch_theme"><input class="switch__input js-switch-theme" type="checkbox" /><span class="switch__in"><span class="switch__box"></span><span class="switch__icon"><svg class="icon icon-moon">
                <use xlink:href="img/sprite.svg#icon-moon"></use>
              </svg><svg class="icon icon-sun">
                <use xlink:href="img/sprite.svg#icon-sun"></use>
              </svg></span></span></label>
      </div>
    </div>
   <script>
   $('#login').on('click',function(){
     $.post( "<?= $urlLogin ?>",{user:$('#user').val(),password:$('#password').val()}, function(data) {
      if(data.success){
        window.location.href = "<?=$urlRedirect?>";
      }else{
        alert( "fail" );
      }
      
    });
   
});
$('.localisation').on('click',function(){
      $.get("<?=url()->toRoute('localisation/change')?>/"+$(this).attr('code'),function(){
        location.reload();
      });
    });
</script>
  </body>
</html>